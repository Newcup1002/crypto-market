<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use GuzzleHttp\Client;

class CreateCoinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        
        Schema::create('coin', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('symbol');
        });

        $this->data();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin');
    }

    public function data()
    {
        $response = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker'), true);
        foreach($response as $coin) {
            \DB::table('coin')->insert([
                'name' => $coin['name'], 'symbol' => $coin['symbol']
            ]);
        }
       
    }
}
