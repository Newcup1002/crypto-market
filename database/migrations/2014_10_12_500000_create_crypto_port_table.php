<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoPortTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('crypto_ports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('crypto_id');
            $table->double('rate', 8, 2);
            $table->double('amount', 8, 2);
            $table->string('curency')->default('USD');
            $table->enum('type', ['buy', 'sell'])->default('buy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_ports');
    }
}
