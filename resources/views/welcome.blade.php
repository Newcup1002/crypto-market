@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Coins Market</div>

                <div class="panel-body">
                    <div id="display-coin">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Market</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Change</th>
                                    <th>Average</th>
                                    <th>Volume</th>
                                </tr>
                            </thead>
                            <tbody id="show-market"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
(function($){
    // Initial
    getCoinMarket();

    setInterval(function(){
        getCoinMarket()
    }, 5000)
})(jQuery)
function getCoinMarket() {
    $.get('https://api.coinmarketcap.com/v1/ticker/', function(resp){
        $('#show-market').html('');
        $.each(resp, function(index, value){
            let data = '';
            if (value.percent_change_24h >= 0) { 
                data = '<tr>\n\
                    <th>'+ ++index +'</th>\n\
                    <th><a href="{{ asset('crypto') }}/'+ value.id +'">'+ value.symbol +'</a></th>\n\
                    <th>'+ value.name +'</th>\n\
                    <th>'+ value.price_usd +'</th>\n\
                    <th><span class="label label-success">'+ value.percent_change_24h +'</span></th>\n\
                    <th>'+ value.total_supply +'</th>\n\
                    <th>'+ value['24h_volume_usd'] +'</th>\n\
                </tr>';
            } else {
                data = '<tr>\n\
                    <th>'+ ++index +'</th>\n\
                    <th><a href="{{ asset('crypto') }}/'+ value.id +'">'+ value.symbol +'</a></th>\n\
                    <th>'+ value.name +'</th>\n\
                    <th>'+ value.price_usd +'</th>\n\
                    <th><span class="label label-danger">'+ value.percent_change_24h +'</span></th>\n\
                    <th>'+ value.total_supply +'</th>\n\
                    <th>'+ value['24h_volume_usd'] +'</th>\n\
                </tr>'
            }
            $('#show-market').append(data);
        });
    });  
}
</script>
@endsection
