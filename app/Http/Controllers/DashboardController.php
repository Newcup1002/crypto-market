<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User;

class DashboardController extends Controller
{

    public $perPage = 15;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!isAdmin()) {
            return redirect('/');
        }
        $users = User::where('permission', 'user')->paginate($this->perPage);
        return view('dashboard')->with([
            'users' => $users
        ]);
    }

    public function update(Request $request, $id)
    {
        if ($request->has('status')) {
            User::where('id', $id)->update(['status' => $request->status]);
        }
        return redirect()->route('dashboard.index');
    }
}
