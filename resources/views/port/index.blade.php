@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">My Portfolio</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-inline" id="formPort">
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                                    <div class="input-group">
                                      <select name="crypto" id="crypto" class="form-control">
                                          <option value="">------</option>
                                          @foreach($data['coin'] as $coin)
                                          <option value="{{ $coin->name }}">{{ $coin->name .'/'. $coin->symbol }}</option>
                                          @endforeach
                                      </select>
                                    </div>
                                </div>
                                <button type="button" id="add-port" class="btn btn-primary">+ Add port</button>
                            </form>
                        </div>
                    </div>
                    <div class="row" id="panel-port">
                        @if(!empty($data['ports']))
                        @foreach($data['ports'] as $myPorts)
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">{{ $myPorts[0]->coin->name }}<span class="pull-right">{{ $data['curentCoin'][$myPorts[0]->crypto_id - 1]->price_usd}}</span></div>
                              <div class="panel-body">
                                <div>
                                    <ul class="list-group" id="list-{{ $myPorts[0]->id }}">
                                        <?php 
                                            $price = $data['curentCoin'][$myPorts[0]->crypto_id - 1]->price_usd;
                                        ?>
                                        @foreach($myPorts as $port)
                                        <?php $profit = $price - $port->rate; ?>
                                        <li class="list-group-item {{ $profit > 0 ? 'list-group-item-success' : 'list-group-item-danger' }}">
                                            <div class="row">
                                                <div class="col-md-3">Buy: {{ ($port->amount / $port->rate) }} {{ $port->curency }}</div>
                                                <div class="col-md-3">Rate: {{ $port->rate }} $</div>
                                                <div class="col-md-3">Amount: {{ $port->amount }} $</div>
                                                <div class="col-md-3">Profit: {{ $profit }} $</div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div id="form-crypto-{{ $myPorts[0]->id }}">
                                    <form class="form-horizontal" id="crypto-currency-{{ $myPorts[0]->id }}">
                                        <div class="form-group">
                                            <label for="price-crypto-{{ $myPorts[0]->id }}" class="col-sm-2 control-label">Price per {{ $myPorts[0]->coin->name }}</label>
                                            <div class="col-sm-10">
                                              <input type="number" name="rate" class="form-control" id="price-crypto-{{ $myPorts[0]->id }}" placeholder="Price per {{ $myPorts[0]->coin->name }}" required="required" value="{{ $data['curentCoin'][$myPorts[0]->crypto_id - 1]->price_usd }}" onkeyup="actionCalculator({{ $myPorts[0]->id }})">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="total-crypto-{{ $myPorts[0]->id }}" class="col-sm-2 control-label">Total {{ $myPorts[0]->id }}</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <input type="number" name="total" class="form-control handle-keyUp" id="total-crypto-{{ $myPorts[0]->id }}" placeholder="Total" value="" required="required" onkeyup="actionCalculator({{ $myPorts[0]->id }})" min="0" step="1">
                                                    <div class="input-group-addon">$</div>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="price-crypto-{{ $myPorts[0]->id }}" class="col-sm-2 control-label">You will receipt</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <input type="number" class="form-control" id="receipt-crypto-{{ $myPorts[0]->id }}" value="0" disabled>
                                                    <div class="input-group-addon">{{ $myPorts[0]->coin->symbol }}</div>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                              <button type="button" class="btn btn-default" onclick="actionCreatePort({{ $myPorts[0]->id }})">Create</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
(function($){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "3000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
    $('#add-port').on('click', function () {
        var $btn = $(this).html('<i class="fa fa-refresh fa-spin"></i>')

        if ($('#crypto').val() !== '' ) {
            var name = $('#crypto').val();

            $.get('https://api.coinmarketcap.com/v1/ticker/' + name.replace(' ', '-').toLowerCase() + '/', function(resp){
                var crypto = resp[0];
                if ($('#form-crypto-' + crypto.rank ).val() === undefined) {
                    $('#panel-port').append(template(crypto))
                    $('#form-crypto-'+ crypto.rank).append(formTemplate(crypto)) 
                }
                clear() 
            });
        } else {
           clear()
        }
    });

})(jQuery)

function actionCreatePort(id) {
    var form = $('#crypto-currency-'+id);
    // $('#crypto-currency-'+id)[0].reset();
    var rate = form.find('input[name="rate"]').val();
    var amount = form.find('input[name="total"]').val();

    $.ajax({
        type: "POST",
        url: "{{ url('crypto-port') }}",
        data: {
          id,
          rate,
          amount,
          type: 'buy'
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: (data) => {
            console.log(data)
            if (data.status == 200) {
                var list = '<li class="list-group-item">\n\
                    <div class="row">\n\
                        <div class="col-md-3">Buy: ' +data.data.amount / data.data.rate +' ' +data.data.curency +'</div>\n\
                        <div class="col-md-3">Rate: '+ data.data.rate +' $</div>\n\
                        <div class="col-md-3">Amount: '+ data.data.amount +' $</div>\n\
                        <div class="col-md-3">Profit: 0 $</div>\n\
                    </div>\n\
                </li>';
                $('#list-'+ data.data.crypto_id).append(list)
                form[0].reset()
            } else if (data.status == 500) {
                toastr["warning"](data.errors.message, "Warning");
            }
            
        }
    });

}

function actionCalculator(id) {
    var perPrice = $('#price-crypto-'+id).val();
    var total = $('#total-crypto-'+id).val();
    $('#receipt-crypto-'+id).val( total/perPrice );
}

function clear() {
    $('#crypto').val('')
    $('#add-port').html('+ Add port');
}

function template(crypto) {
    var {
        name,
        rank
    } = crypto
    return '<div class="col-md-12">\n\
        <div class="panel panel-default">\n\
          <div class="panel-heading">'+ name +'</div>\n\
          <div class="panel-body">\n\
            <div>\n\
                <ul class="list-group" id="list-'+ rank +'"></ul>\n\
            </div>\n\
            <div id="form-crypto-'+ rank +'"></div>\n\
          </div>\n\
        </div>\n\
    </div>';
}

function formTemplate(coin) {
    var {
        name,
        rank,
        price_usd,
        symbol
    } = coin
    return '<form class="form-horizontal" id="crypto-currency-'+ rank +'">\n\
      <div class="form-group">\n\
        <label for="price-crypto-'+ rank +'" class="col-sm-2 control-label">Price per '+ name +'</label>\n\
        <div class="col-sm-10">\n\
          <input type="number" name="rate" class="form-control" id="price-crypto-'+ rank +'" placeholder="Price per '+ name +'" required="required" value="'+ price_usd +'">\n\
        </div>\n\
      </div>\n\
      <div class="form-group">\n\
        <label for="total-crypto-'+ rank +'" class="col-sm-2 control-label">Total</label>\n\
        <div class="col-sm-10">\n\
            <div class="input-group">\n\
                <input type="number" name="total" class="form-control handle-keyUp" id="total-crypto-'+ rank +'" placeholder="Total" value="" required="required" onkeyup="actionCalculator('+ rank +')" min="0" step="1">\n\
                <div class="input-group-addon">$</div>\n\
            </div>\n\
        </div>\n\
      </div>\n\
      <div class="form-group">\n\
        <label for="price-crypto-'+ rank +'" class="col-sm-2 control-label">You will receipt</label>\n\
        <div class="col-sm-10">\n\
            <div class="input-group">\n\
                <input type="number" class="form-control" id="receipt-crypto-'+ rank +'" value="0" disabled>\n\
                <div class="input-group-addon">'+ symbol +'</div>\n\
            </div>\n\
        </div>\n\
      </div>\n\
      <div class="form-group">\n\
        <div class="col-sm-offset-2 col-sm-10">\n\
          <button type="button" class="btn btn-default" onclick="actionCreatePort('+rank+')">Create</button>\n\
        </div>\n\
      </div>\n\
    </form>';
}
</script>
@endsection
