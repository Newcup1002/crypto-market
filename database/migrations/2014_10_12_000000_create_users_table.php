<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('permission', ['admin', 'user'])->default('user');
            $table->boolean('status');
            $table->rememberToken();
            $table->timestamps();
        });

        // Initial data admin
        $this->data();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

    public function data()
    {
        \DB::table('coin')->insert([
            'name' => 'Crypto Admin',
            'email' => 'admin@email.com',
            'password' => bcrypt('admin1234'),
            'permission' => 'admin',
            'status' => 1,
        ]);
    }
}
