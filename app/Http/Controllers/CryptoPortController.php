<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Coin;
use App\Models\CryptoPort;

class CryptoPortController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $client = new Client();
        $response = $client->request('get', 'https://api.coinmarketcap.com/v1/ticker/');
        $data['curentCoin'] = json_decode($response->getBody());

        $data['coin'] = Coin::get();
        $data['ports'] = CryptoPort::where('user_id', \Auth::user()->id)
            ->orderBy('crypto_id', 'asc')
            ->get()
            ->groupBy('crypto_id');
        return view('port.index')->with('data', $data);
    }

    public function show($crypto)
    {

        return view('port.index')->with(['crypto' => $crypto]);
    }

    public function store(Request $request)
    {
        if (!isAdmin()) {
            return response()->json([
                'status' => 500,
                'errors' => [
                    'message' => 'Permission is locked!'
                ]
            ]);
        }

        $coin = Coin::find($request->id);

        if ($coin) {
            $port = CryptoPort::create([
                'user_id' => \Auth::user()->id,
                'crypto_id' => $request->id,
                'rate' => $request->rate,
                'amount' => $request->amount,
                'curency' => $coin->symbol,
                'type' => $request->type
            ]);

            return response()->json([
                'status' => 200,
                'data' => $port
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'errors' => []
            ]);
        }
        
    }
}
