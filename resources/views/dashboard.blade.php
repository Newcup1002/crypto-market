@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
            	<div class="panel-body">
	        		<table class="table table-hover">
	        			<thead>
	        				<tr>
	        					<th style="text-align: center">No.</th>
	        					<th style="text-align: center">Name</th>
	        					<th style="text-align: center">Email</th>
	        					<th style="text-align: center">Status</th>
	        					<th style="text-align: center">Action</th>
	        				</tr>
	        			</thead>
	        			<tbody>
	        				<?php $perPage = $users->perPage(); ?>
	        				@foreach($users as $key => $user)
	        				<tr>
	        					<td>{{ isset($_GET['page']) ? $perPage + $key  + 1 : $key  + 1 }}</td>
	        					<td>{{ $user->name }}</td>
	        					<td>{{ $user->email }}</td>
	        					<td>{{ $user->status }}</td>
	        					<td>
	        						@if(!$user->status)
	        						<form action="{{ route('dashboard.update', $user->id) }}" method="POST">
	        							{!! csrf_field() !!}
                    					<input type="hidden" name="_method" value="PUT">
                    					<input type="hidden" name="status" value="1">
	        							<button type="submit" class="btn btn-primary">Active</button>
	        						</form>
	        						@else
	        						<form action="{{ route('dashboard.update', $user->id) }}" method="POST">
	        							{!! csrf_field() !!}
                    					<input type="hidden" name="_method" value="PUT">
                    					<input type="hidden" name="status" value="0">
	        							<button type="submit" class="btn btn-danger">Inactive</button>
	        						</form>
	        						@endif
	        					</td>
	        				</tr>
	        				@endforeach
	        			</tbody>
	        		</table>
	        		<div class="text-right">{{ $users->links() }}</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
(function($){
    

})(jQuery)

</script>
@endsection
