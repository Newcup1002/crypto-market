<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transection extends Model
{
    protected $table = 'transection';
    protected $primaryKey = 'id';
    protected $timestamps = true;

    protected $fillable = [
        'user_id',
        'currency_id',
        'rate',
        'amount',
        'type'
    ];

    public function User()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
