<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    protected $table = 'wallet';
    protected $primaryKey = 'id';
    protected $timestamps = true;

    protected $fillable = [
        'user_id',
        'currency_id',
        'total',
        'type'
    ];

    public function User()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
