@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Feature coming soon!</h1>
            <div class="panel panel-default">
                <div class="panel-heading" id="coin-title"></div>

                <div class="panel-body">
                    <div>
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#buy" aria-controls="buy" role="tab" data-toggle="tab">Buy</a></li>
                        <li role="presentation"><a href="#sell" aria-controls="sell" role="tab" data-toggle="tab">Sell</a></li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="buy">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div id="display-coin"></div>
                                </div>
                                <div class="col-sm-6 col-md-8">
                                    <form action="#" class="form-horizontal" method="post">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Balance :</label>
                                            <div class="col-sm-10 control-label">
                                                0
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="sell">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div id="display-coin"></div>
                                </div>
                                <div class="col-sm-6 col-md-8">
                                    <form action=""></form>
                                </div>
                            </div>
                        </div>
                      </div>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
(function($){
    // Initial
     $.get('https://api.coinmarketcap.com/v1/ticker/{{ $crypto }}/', function(resp){
        console.log(resp)
        $('#coin-title').html(resp[0].name)
    }); 

})(jQuery)
</script>
@endsection
