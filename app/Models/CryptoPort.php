<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CryptoPort extends Model
{
    protected $table = 'crypto_ports';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'crypto_id',
        'rate',
        'amount',
        'curency',
        'type'
    ];

    public function User()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function coin()
    {
        return $this->hasOne('App\Models\Coin','id','crypto_id');
    }
}
